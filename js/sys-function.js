/*globals $, alert*/
var day = 0, hour = 0, minute = 0, second = 0;

function printTime() {
    "use strict";
    var n, t;
    n = new Date();
    t = new Date((new Date("2015/06/07 09:00:00")).getTime());
    if ((t - n) >= 0) {
        day = Math.floor((t - n) / 1000 / 86400);
        hour = Math.floor((t - n) / 1000 % 86400 / 3600);
        minute = Math.floor((t - n) / 1000 % 3600 / 60);
        second = Math.floor((t - n) / 1000 % 60);
    }
    if (second < 10) {second = "0" + second; }
    if (minute < 10) {minute = "0" + minute; }
    if (hour < 10) {hour = "0" + hour; }
    $("#day").html(day);
    $("#hour").html(hour);
    $("#minute").html(minute);
    $("#second").html(second);
}

function frameLeave() {
    "use strict";
    $(".content > div").hide();
}

function frameLoad(x) {
    "use strict";
    frameLeave();
    var f = $("#section" + x), limit = 10;
    switch (x) {
    case 1:
        limit = 5;
        f.find(".content").css("background-image", "url('image/s1bg1.png')");
        setTimeout(function () {
            $("#s1p4 div").fadeIn();
        }, 2000);
        setTimeout(function () {
            $("#s1p5 div").fadeIn();
        }, 2500);
        setInterval(function () {printTime(); }, 1000);
        break;
    case 2:
        limit = 5;
        f.find(".content").css("background-image", "url('image/s2bg1.png')");
        if (f.find("#s2p5 .cover").attr("paused") === "2") {
            f.find("#s2p5 .cover").click();
        }
        break;
    case 3:
        limit = 6;
        break;
    case 4:
        limit = 3;
        break;
    }
    f.find(".content").children("div").each(function (index) {
        if (index < limit) {
            $(this).delay(500 * index).fadeIn();
        }
    });
}

$(function () {
    "use strict";
    frameLoad(1);
    $("#fullpage").fullpage({
        afterLoad: function (anchorLink, index) {
            frameLoad(index);
        }
    });
    $('#manka').perfectScrollbar();
    $('#chart').perfectScrollbar();
});

$(".manka").click(function () {
    "use strict";
    switch ($(this).attr("id")) {
    case "s3p2":
        $("#manka .img").html('<img src="comics/comic1.jpg" />');
        break;
    case "s3p3":
        $("#manka .img").html(
            '<img src="comics/comic2_1.gif" />' +
                '<img src="comics/comic2_2.gif" />' +
                '<img src="comics/comic2_3.gif" />' +
                '<img src="comics/comic2_4.gif" />' +
                '<img src="comics/comic2_5.gif" />'
        );
        break;
    case "s3p4":
        $("#manka .img").html(
            '<img src="comics/comic3_1.jpg" />' +
                '<img src="comics/comic3_2.jpg" />' +
                '<img src="comics/comic3_3.jpg" />' +
                '<img src="comics/comic3_4.jpg" />' +
                '<img src="comics/comic3_5.jpg" />' +
                '<img src="comics/comic3_6.jpg" />' +
                '<img src="comics/comic3_7.jpg" />' +
                '<img src="comics/comic3_8.jpg" />'
        );
        break;
    case "s3p5":
        $("#manka .img").html(
            '<img src="comics/comic4_1.jpg" />' +
                '<img src="comics/comic4_2.jpg" />' +
                '<img src="comics/comic4_3.jpg" />' +
                '<img src="comics/comic4_4.jpg" />'
        );
        break;
    }
    $('#manka').perfectScrollbar('update');
    $("#manka").show();
    $.fn.fullpage.setAllowScrolling(false);
    $("body").css("overflow", "hidden");
});

$("#manka .img").click(function () {
    "use strict";
    $("#manka").scrollTop(0);
    $("#manka").hide();
    $.fn.fullpage.setAllowScrolling(true);
});

$(".chart").click(function () {
    "use strict";
    $("#chart img").attr("src", "chart/chart1.jpg");
    $('#chart').perfectScrollbar('update');
    $("#chart").show();
    $.fn.fullpage.setAllowScrolling(false);
    $("body").css("overflow", "hidden");
});

$("#chart .img").click(function () {
    "use strict";
    $("#chart").scrollTop(0);
    $("#chart").hide();
    $.fn.fullpage.setAllowScrolling(true);
});

$(".cover").click(function () {
    "use strict";
    var s = $(this).attr("paused");
    switch (s) {
    case "2"://stopped
        $(".cover").attr("paused", "2");
        $(".cover").css("background-image", "");
        $(this).attr("paused", "0");
        $(this).css("background-image", "url(image/play.png)");
        $("audio").attr("src", $(this).attr("src"));
        $("audio")[0].play();
        break;
    case "1"://paused
        $(this).attr("paused", "0");
        $(this).css("background-image", "url(image/play.png)");
        $("audio")[0].play();
//        $(this).siblings("audio").play();//wrong
        break;
    case "0"://playing
        $(this).attr("paused", "1");
        $(this).css("background-image", "url(image/pause.png)");
        $("audio")[0].pause();
        break;
    }
});

$("audio")[0].onended = function (e) {
    "use strict";
    $(".cover").attr("paused", "2");
    $(".cover").css("background-image", "");
};